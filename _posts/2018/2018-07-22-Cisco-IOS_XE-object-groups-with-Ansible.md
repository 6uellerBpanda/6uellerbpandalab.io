---
categories: ["Automation"]
title:  "Cisco IOS-XE object-groups with Ansible"
description: "Automate object-groups creation on Cisco IOS-XE"
image: "/assets/img/traffic_light_marx.jpg"
render_with_liquid: false
tags:
  - cisco
  - ansible
---


Some small snippets to make our ACL life more easier.

* TOC
{:toc}

## Create a network object-group for every VLAN

{% raw %}
~~~ yaml
# network.yml
switch_object_groups_vlan: "{{ switch_network }}"

switch_network:
  server:
    vlan_id: 10
    network_address: '10.0.10.0'
    broadcast_address: '10.0.10.255'
    subnetmask: '255.255.255.0'
    subnetmask_prefix: '/24'
    gateway: '10.0.10.254'
  storage:
    vlan_id: 11
    network_address: '10.0.11.0'
    broadcast_address: '10.0.11.127'
    subnetmask: '255.255.255.128'
    subnetmask_prefix: '/25'
    network_cidr: '10.0.11.0/25'


# configure_object_groups.yml
- name: configure vlan network object groups
  ios_config:
    lines: "{{ item.value.network_address }} {{ item.value.subnetmask }}"
    parents: "object-group network {{ 'NET-VLAN-' ~ item.key | upper }}"
    before: "no object-group network {{ 'NET-VLAN-' ~ item.key | upper }}"
    match: 'line'
    replace: 'line'
  with_dict: "{{ switch_object_groups_vlan | default({}) }}"
~~~
{% endraw %}

This will create following network object-groups:

~~~ conf
# switch
Network object group NET-VLAN-SERVER
 10.0.10.0 255.255.255.0

Network object group NET-VLAN-STORAGE
 10.0.11.0 255.255.255.128
~~~

## Create network object-groups

{% raw %}
~~~ yaml
# object_groups.yml
dns_server:
  - '10.0.10.10'
  - '10.0.10.11'
  - '8.8.8.8'
  - '8.8.4.4'

switch_object_groups_network:
  - name: net-dns-internal
    lines:
      - "host {{ dns_server[0] }}"
      - "host {{ dns_server[1] }}"
  - name: net-dns-external
    lines:
      - "host {{ dns_server[2] }}"
      - "host {{ dns_server[3] }}"

# configure_object_groups.yml
- name: configure network object groups
  ios_config:
    lines: "{{ item.lines | flatten }}"
    parents: "object-group network {{ item.name | upper }}"
    before: "no object-group network {{ item.name | upper }} "
    match: 'line'
    replace: 'line'
  with_items: "{{ switch_object_groups_network | default([]) }}"
~~~
{% endraw %}

This will create following network object-groups:

~~~ conf
# switch
Network object group NET-DNS-EXTERNAL
 host 8.8.8.8
 host 8.8.4.4

Network object group NET-DNS-INTERNAL
 host 10.0.10.10
 host 10.0.10.11
~~~

## Create service object-groups

{% raw %}
~~~ yaml
# object_groups.yml
switch_object_groups_service:
  - name: srvc-http-s
    lines: ['tcp eq www', 'tcp eq 443']
  - name: srvc-zimbra
    lines:
      - ['tcp eq 22', 'tcp eq smtp', 'tcp eq 389', 'tcp eq 3310', 'tcp eq 11211']
      - ['udp eq domain', 'udp eq syslog', 'udp eq 11211']

# configure_object_groups.yml
- name: configure service object groups
  ios_config:
    lines: "{{ item.lines | flatten }}" # with flatten you can make the data more compact
    parents: "object-group service {{ item.name | upper }}"
    before: "no object-group service {{ item.name | upper }} "
    match: 'line'
    replace: 'line'
  with_items: "{{ switch_object_groups_service | default([]) }}"
~~~
{% endraw %}

This will create following service object-groups:

~~~ conf
# switch
Service object group SRVC-HTTP-S
 tcp eq www
 tcp eq 443

Service object group SRVC-ZIMBRA
 tcp eq 22
 tcp eq smtp
 tcp eq 389
 tcp eq 3310
 tcp eq 11211
 udp eq domain
 udp eq syslog
 udp eq 11211
~~~

---
Tested with:

* Ansible 2.5.6
* Cisco Catalyst 4500X, IOS-XE 03.08.06

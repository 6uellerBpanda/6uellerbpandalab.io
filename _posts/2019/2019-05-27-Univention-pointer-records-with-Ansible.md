---
title:  "Univention pointer records with Ansible"
description: "Automate reverse lookup records in Univention Corporate Server"
categories: ["Automation"]
image: "/assets/img/reverse.jpg"
tags:
  - univention
  - ansible
render_with_liquid: false
---

A snippet how I dealt with the [udm cli](https://docs.software-univention.de/manual-4.4.html#central:udm) of [Univention Corporate Server](https://www.univention.com/products/ucs/) to create PTR records.

* TOC
{:toc}

## Goal
Create for every device a PTR record

## Data
I have following data from where I will create the pointer:

~~~ yaml
devices:
  workstation:
    linux-1:  { name: 'linux-1', user: 'test1', ip_addr: '10.0.20.3' }
    linux-2:  { name: 'linux-2', user: 'test2', ip_addr: '10.0.20.4' }
  laptop:
    nb-linux-1: { name: 'nb-linux-1', user: 'test1', ip_addr: '10.0.30.10' }
    nb-linux-2: { name: 'nb-linux-2', user: 'test1', ip_addr: '10.0.30.11' }
~~~

## List all PTR with udm command
The output of the udm command to list PTR records looks like this:

~~~ shell_session
root@srv-ucs-01:/root# udm dns/ptr_record list
DN: relativeDomainName=3,zoneName=20.0.10.in-addr.arpa,cn=dns,dc=example,dc=com
  address: 3
  ip: 10.0.20.3
  ptr_record: linux-1.example.com.
~~~

Last octet of the IP is the *relativeDomainName*.

The *zoneName* is the reverse lookup zone.

## Register current PTR
First we register all current configured records

~~~ yaml
- name: list dns ptr records
  shell: >
    udm dns/ptr_record list
  args:
    chdir: '/usr/sbin'
  register: dns_ptr_records
  changed_when: false
  ignore_errors: true
~~~

## Create PTR via ipaddr filter
The syntax to create the PTR via udm is following:

~~~ shell_session
root@srv-ucs-01:/root# udm dns/ptr_record create --superordinate "zoneName=20.0.10.in-addr.arpa,cn=dns,dc=example,dc=com" \
--set ip="10.0.20.3" --set ptr_record="linux-1.example.com."
~~~

Ansible task:

~~~ yaml
- name: create ptr records
  shell: >
    udm dns/ptr_record create
    --superordinate "zoneName={{ item.ip_addr | ipaddr('revdns') | regex_replace('^\d+\D(.*).$', '\1') }},cn=dns,dc=example,dc=com"
    --set ip="{{ item.ip_addr }}" --set ptr_record="{{ item.name }}.dc=example,dc=com."
  args:
    chdir: '/usr/sbin'
  loop: "{{ devices | json_query('*.*[]') }}" # traverse the nested hash to get the right data
  when: dns_ptr_records.stdout.find(item.name) == -1 # only create when the name is not found in the list output
~~~

The `ipaddr('revdns')` would give us this: "3.20.0.10.in-addr.arpa.".
To make this compatible with udm syntax I remove the unwanted stuff with the `regex_replace` filter.

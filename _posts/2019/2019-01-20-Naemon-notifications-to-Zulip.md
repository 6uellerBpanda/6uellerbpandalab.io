---
title:  "Naemon notifications to Zulip"
description: "Send Naemon notifications to Zulip"
categories: ["Monitoring"]
image: "/assets/img/monitoring.jpg"
tags:
  - zulip
  - naemon
  - omd
---

[Zulip](https://zulipchat.com) already has a [Nagios integration](https://zulipchat.com/integrations/doc/nagios) which is also working for Naemon but every notification is creating it's own topic and this what I want to avoid.
Due to Zulip's API this can also be achieved without changing the Nagios integration.

* TOC
{:toc}

## Requirements
It's also possible to send a curl command but I decided to use `zulip-send` 'cause of the "cleaner command".

To get `zulip-send` it's necessary to install the Zulip python bindings on the Naemon host:

~~~ shell
pip install zulip
~~~

## Zulip
Now it's time to [create a bot](https://zulipchat.com/help/add-a-bot-or-integration).
Let's call it 'monitoring-bot@example.com'. You can then download the `zuliprc` file and put it into the home folder (~/.zuliprc) of the naemon user.

In my case I'm using [OMD](https://github.com/ConSol/omd) with Naemon backend and the OMD site is called *internal*.

~~~ shell
OMD[internal@srv-mon-01]:~$ cat ~/.zuliprc
[api]
email=monitoring-bot@example.com
key=1234
site=https://zulip.example.com
~~~

## Naemon
The service notification command:

~~~ conf
# etc/naemon/conf.d/notification_command_zulip.cfg
# multiline messages are a little tricky - carriage return is necessary to output newlines
define command {
    command_name notify-service-by-zulip
    command_line /usr/local/bin/zulip-send --stream <STREAM> --subject <TOPIC> \
        --message "`echo \"**$NOTIFICATIONTYPE$**: $HOSTNAME$\r\n**Service**: $SERVICEDESC$ is $SERVICESTATE$\r\n\n**Description**\r\n$SERVICEOUTPUT$\"`"
}
~~~

After changing the `service_notification_command` at the contact object and a naemon restart this will produce following Zulip message:

![test](../../../assets/img/naemon_zulip_notification.jpg)

Of course the same can be applied host notifications and different OMD sites.

---
Tested with:

* Naemon 1.0.9
* Zulip 1.9.1

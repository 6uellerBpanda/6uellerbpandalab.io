---
title:  "Ansible role for Zulip"
description: "Deploy Zulip via Ansible"
categories: ["Automation"]
image: "/assets/img/zulip_octupus.jpg"
tags:
  - zulip
  - ansible
---

I'm using [Zulip](https://zulipchat.com) already for a few months and decided to share the Ansible role.

* TOC
{:toc}

## Requirements
The role only supports Debian and Zulip version >2.0

## Features
So far following is automated:

* Downloading the Zulip package and executing the [Zulip install script](https://zulip.readthedocs.io/en/stable/production/install.html#step-2-install-zulip)
* LDAP/AD configuration
* Push server registraion
* Stream creation via bot (bot needs to created before manually)
* and other various things

You can check it out here: <https://gitlab.com/6uellerBpanda/ansible-role-zulip>

---
title:  "OMD Labs servicegroups creation"
description: "Automate servicegroups via Ansible role"
image: "/assets/img/monkey_island_group.jpg"
categories: ["Monitoring"]
tags:
  - omd
  - ansible
render_with_liquid: false
---

I will post some articles about how to use the [OMD Ansible role](https://gitlab.com/6uellerBpanda/ansible-role-omd) and will start
with [service groups](http://www.naemon.org/documentation/usersguide/objectdefinitions.html#servicegroup).

* TOC
{:toc}

## Hosts
We've following hosts defined in the Ansible inventory:

~~~ conf
[linux]
smtp.example.com
imap.example.com
dns.example.com
~~~

## Add hosts from the Ansible inventory
We need to add this group of hosts into Naemon via the `omd_naemon_hosts_hash`

~~~ yaml
omd_site_hash:
  - name: test
    omd_naemon_hosts_hash:
    linux:
      # enable_inventory_hostname: true # set this if you don't gather facts from these hosts
~~~

This will create some host objects at *etc/naemon/conf.d/hosts.cfg*

~~~ conf
define host {
    host_name    smtp
    address      smtp.example.com
    use          generic-host
}

define host {
    host_name    imap
    address      imap.example.com
    use          generic-host
}

define host {
    host_name    dns
    address      dns.example.com
    use          generic-host
}
~~~

## Configure service
I will add a simple `check_disk` service object:

~~~ yaml
omd_naemon_services_hash:
  check_disk:
    host_name: 'smtp,imap,dns'
    check_command: 'check_disk!-w 10%!-c 5%'
    check_interval: '60'
~~~

The service will be added to *etc/naemon/conf.d/services.cfg*

~~~ conf
define service {
    host_name           smtp,imap,dns
    service_description check_disk
    check_command       check_disk!-w 10%!-c 5%
    check_interval      60
    use                 generic-service
}
~~~

## Add linux servicegroup
Everything is prepared to create a servicegroup now. I will add every service (with *) defined on these hosts to the servicegroup.

{% raw %}
~~~ yaml
omd_naemon_servicegroups_hash:
  linux:
    members: "{{ groups['linux'] | map('extract', hostvars, ['ansible_hostname']) | join(', *, ') }}, *"
    members: "{{ groups['linux'] | join(', *, ') }}, *" # a servicegroup without ansible facts
~~~
{% endraw %}

The outcome will be a servicegroup called *linux* at *etc/naemon/conf.d/servicegroups.cfg*

~~~ conf
define servicegroup {
  servicegroup_name linux
  alias             linux
  members           smtp, *, imap, *, dns, *
}
~~~

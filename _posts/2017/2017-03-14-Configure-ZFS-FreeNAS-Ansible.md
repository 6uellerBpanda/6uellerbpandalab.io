---
title:  "Configure ZFS on FreeNAS with Ansible"
description: "Use the Ansible zfs module to automate zfs operations on FreeNAS"
categories: ["Automation"]
tags:
  - zfs
  - freenas
  - ansible
---

With the Ansible [zfs module](http://docs.ansible.com/ansible/zfs_module.html) we can configure some zfs parts on FreeNAS.

* TOC
{:toc}

## Ansible

### Inventory
We need to pass down some variables to the host/group in order to get Ansible working.

~~~ conf
# hosts file
[freenas]
sto-nas-01.test.at

[freenas:vars]
ansible_user=root
# root user has csh as default shell
ansible_shell_type=csh
# path to python
ansible_python_interpreter=/usr/local/bin/python
~~~


### Connect to FreeNAS
Let's start with a simple *setup* command to test basic connectivity.

~~~ shell
ansible sto-nas-01.test.at -m setup -a 'filter=ansible_product*'
sto-nas-01.test.at | SUCCESS => {
    "ansible_facts": {
        "ansible_product_name": "FREENAS-CERTIFIED-20V2-64",
        "ansible_product_serial": "<SERIAL>",
        "ansible_product_uuid": "00000000-0000-0000-0000-0CC47A541B58",
        "ansible_product_version": "1234567890"
    },
    "changed": false
}
~~~


### Creating a FreeNAS role
I will use following simple directory layout for testing purpose:

~~~ conf
hosts
playbooks/
  freenas.yml
roles/
  freenas/
    defaults/main.yml
    tasks/main.yml
~~~

The task will configure a dataset with some zfs properties (you can use any zfs property) and some default values set.

{% raw %}
~~~ yml
# tasks/main.yml
- name: configure dataset
  zfs:
    name: "{{ item.key }}"
    state: "{{ item.value.ensure | default(freenas_zfs_ensure) }}"
    atime: "{{ item.value.atime | default(freenas_zfs_property_atime) }}"
    primarycache: "{{ item.value.primarycache | default(freenas_zfs_property_primarycache) }}"
    secondarycache: "{{ item.value.secondarycache | default(freenas_zfs_property_secondarycache) }}"
    logbias: "{{ item.value.logbias | default(freenas_zfs_property_logbias) }}"
    refquota: "{{ item.value.refquota | default(freenas_zfs_property_refquota) }}"
    sync: "{{ item.value.sync | default(freenas_zfs_property_sync) }}"
  with_dict: "{{ freenas_zfs_dataset_hash | default({}) }}"
~~~
{% endraw %}

These are the default values for the freenas role.

~~~ yml
# defaults/main.yml
freenas_zfs_ensure: 'present'
freenas_zfs_property_atime: 'off'
freenas_zfs_property_primarycache: 'none'
freenas_zfs_property_secondarycache: 'none'
freenas_zfs_property_logbias: 'latency'
freenas_zfs_property_refquota: 'none'
freenas_zfs_property_sync: 'standard'
# init hash
freenas_zfs_dataset_hash: {}
~~~

This is the playbook which holds the data. The root dataset *tank* is necessary as it does not resolve dependencies on the datasets.

~~~ yml
# playbooks/freenas.yml
- hosts: freenas

  roles:
    - role: freenas
      freenas_zfs_dataset_hash:
        # root dataset
        tank:
        # vm
        tank/vmfs:
          refquota: '1T'
          primarycache: 'all'
          secondarycache: 'all'
        # home folders
        tank/home:
          refquota: '3T'
        # git repo
        tank/git:
~~~

### Run playbook

~~~ shell
ansible-playbook -D -l freenas playbooks/freenas.yml

PLAY [freenas] *****************************************************************

TASK [setup] *******************************************************************
ok: [sto-nas-01.test.at]

PLAY [sto-nas-01.test.at] *********************************************

TASK [freenas : configure dataset] *********************************************
changed: [sto-nas-01.test.at] => (item={'key': u'tank', 'value': None})
changed: [sto-nas-01.test.at] => (item={'key': u'tank/vmfs', 'value': {u'primarycache': u'all', u'refquota': u'1T', u'secondarycache': u'all'}})
changed: [sto-nas-01.test.at] => (item={'key': u'tank/home', 'value': {u'refquota': u'3T'}})
changed: [sto-nas-01.test.at] => (item={'key': u'tank/git', 'value': None})
PLAY RECAP *********************************************************************
sto-nas-01.test.at : ok=0    changed=4    unreachable=0    failed=0   
~~~

Voilà, ZFS datasets created and zfs properties set.

## FreeNAS

Checking some zfs properties set via Ansible.

~~~ shell
[root@sto-nas-01] ~# zfs get all tank/vmfs
NAME       PROPERTY                 VALUE                    SOURCE
tank/vmfs  refquota                 1T                       local
tank/vmfs  primarycache             all                      local
tank/vmfs  secondarycache           all                      local
tank/vmfs  logbias                  latency                  local
~~~

Tested with:
 - Ansible 2.2.1
 - FreeNAS: 9.10.2

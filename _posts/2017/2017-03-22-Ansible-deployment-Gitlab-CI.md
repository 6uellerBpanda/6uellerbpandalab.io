---
title:  "Ansible deployment with Gitlab CI"
description: "Execute Ansible deployment for different environments via Gitlab CI"
categories: ["Automation"]
tags:
  - gitlab
  - ansible
---

Making an [Ansible](http://www.ansible.com) deployment via [Gitlab CI](https://about.gitlab.com/gitlab-ci/) is easy but also flexible enough to parameterize the deployment.

* TOC
{:toc}

## Requirements
* Gitlab Project with CI enabled
* Gitlab Runner with Ansible, ansible-lint installed
* CI Trigger added to the Ansible project

## Ansible
I will use the [alternative directory layout](http://docs.ansible.com/ansible/playbooks_best_practices.html#alternative-directory-layout) from the ansible best practices:

~~~ conf
# inventory
staging/
  hosts
production/
  hosts

playbooks/
  vcs.yml
roles/
  common/
# top level playbook
site.yml
~~~

## Gitlab
The *.gitlab-ci.yml* in the Ansible git repository/project:

~~~ yml
---
variables:
  # top level playbook
  TLP: 'site.yml'

stages:
  - test
  - deploy

## STAGING
test:
  stage: test
  script:
    - "ansible-lint $TLP"
  environment:
    name: staging

# run top level playbook
deploy_to_stag:
  stage: deploy
  script:
    - "ansible-playbook -b -D -i staging $TLP"
  environment:
    name: staging
  only:
    - master
  when: manual

# run via api call
deploy_trigger_to_stag:
  stage: deploy
  script:
    - "ansible-playbook -b -D -i staging playbooks/$PLAYBOOK.yml -l $HOSTS"
  environment:
    name: staging
  only:
    - triggers

## PROD
deploy_to_prod:
  stage: deploy
  script:
    - "ansible-playbook -b -D -i production $TLP"
  environment:
    name: production
  only:
    - master
  when: manual
~~~

### Jobs

#### test
Everything goes through [ansible-lint](https://github.com/willthames/ansible-lint) first.

#### deploy_to_stag
This is a manual job which starts an Ansible run through all hosts/playbooks for staging env.
No other branches then master go through this job.

#### deploy_trigger_to_stag
This job gets triggered via an [API call](https://docs.gitlab.com/ce/ci/triggers/README.html#trigger-a-job-from-webhook) to Gitlab.
2 variables *$PLAYBOOK* and *$HOSTS* are available to limit the Ansible run.
It is of course also possible to add Ansible tags as variable to have even more flexibility.
All branches including master are allowed.

#### deploy_to_prod
The same as job *deploy_to_stag* but for production environment.

## Start the pipeline to run Ansible deployment
With following API call we start the *deploy_trigger_to_stag* job.

You can either use [Postman](https://www.getpostman.com/) or just a simple curl command.

~~~ shell
curl --request POST --form token=<YOUR_TOKEN> --form ref=master \
--form "variables[PLAYBOOK]=vcs" --form "variables[HOSTS]=srv-vcs-01" https://gitlab.test.at/api/v3/projects/<PROJECT_ID>/trigger/builds
~~~
It will execute the *playbooks/vcs.yml* for host *srv-vcs-01* which is configured in the *staging/hosts* file.

---
title:  "OMD Labs with check_multi"
description: "Automate Port Security configuration"
categories: ["Automation"]
image: "/assets/img/planes_formation.jpg"
tags: ['omd', 'ansible']
---

[check_multi](https://github.com/flackem/check_multi) is a plugin to run multiple checks under one service.

I use it very often so I've added it to the [OMD Ansible role](https://gitlab.com/6uellerBpanda/ansible-role-omd).

* TOC
{:toc}

Let's take for example the [check_pve](https://gitlab.com/6uellerBpanda/check_pve) plugin to combine some checks.

~~~ yaml
omd_naemon_macros_hash:
  USER7: "test1234"

omd_naemon_commands_hash:
 check_multi:
  line: "$USER1$/check_multi -s HOSTADDRESS=$HOSTADDRESS$ -s HOSTNAME=$HOSTNAME$ -s USER7=$USER7$ -f {{ omd_site_dir }}/internal/etc/check_multi/$ARG1$ -r \"$ARG2$\""

omd_naemon_services_hash:
  check_pve_perf:
    hostgroup_name: 'proxmox'
    check_command: 'check_multi!check_pve_perf.cmd!1+2+8'


omd_naemon_check_multi_list:
  - name: check_pve_perf
    check_cpu:
      command: "$USER1$/check_pve.rb -s $HOSTADDRESS$ -u monitoring@pve -p $USER7$ -m cpu -n $HOSTNAME$ -w 70 -c 80"
    check_mem:
      command: "$USER1$/check_pve.rb -s $HOSTADDRESS$ -u monitoring@pve -p $USER7$ -m memory -n $HOSTNAME$ -w 110 -c 120"
    check_net_in:
      command: "$USER1$/check_pve.rb -s $HOSTADDRESS$ -u monitoring@pve -p $USER7$ -m net_in -n $HOSTNAME$ --timeframe hour --cf average -w 256000 -c 307200"
    check_net_out:
      command: "$USER1$/check_pve.rb -s $HOSTADDRESS$ -u monitoring@pve -p $USER7$ -m net_out -n $HOSTNAME$ --timeframe hour --cf average -w 256000 -c 307200"
    check_io_wait:
      command: "$USER1$/check_pve.rb -s $HOSTADDRESS$ -u monitoring@pve -p $USER7$ -m io_wait -n $HOSTNAME$ -w 4 -c 6"
~~~

This will give us following configuration.

~~~ conf
###--- resource.cfg ---###
$USER1$=/omd/sites/test/lib/monitoring-plugins
$USER7$=test1234

###--- commands.cfg ---###
define command {
        command_name    check_multi
        command_line    $USER1$/check_multi -s HOSTADDRESS=$HOSTADDRESS$ -s HOSTNAME=$HOSTNAME$ -s USER1=$USER1$ -s USER7=$USER7$ -f /omd/sites/test/etc/check_multi/$ARG1$ -r "$ARG2$"
}

###--- services.cfg ---###
define service {
    hostgroup_name            proxmox
    service_description       check_pve_perf
    check_command             check_multi!check_pve_perf.cmd!1+2+8
    check_interval            5
    use                       generic-service-new-with-perf
}
~~~

It will also create the check_multi file.

~~~ conf
###--- /omd/sites/test/etc/check_multi/check_pve_perf.cmd
command [check_net_in] = $USER1$/check_pve.rb -s $HOSTADDRESS$ -u monitoring@pve -p $USER7$ -m net_in -n $HOSTNAME$ --timeframe hour --cf average -w 256000 -c 307200
command [check_io_wait] = $USER1$/check_pve.rb -s $HOSTADDRESS$ -u monitoring@pve -p $USER7$ -m io_wait -n $HOSTNAME$ -w 4 -c 6
command [check_mem] = $USER1$/check_pve.rb -s $HOSTADDRESS$ -u monitoring@pve -p $USER7$ -m memory -n $HOSTNAME$ -w 110 -c 120
command [check_net_out] = $USER1$/check_pve.rb -s $HOSTADDRESS$ -u monitoring@pve -p $USER7$ -m net_out -n $HOSTNAME$ --timeframe hour --cf average -w 256000 -c 307200
command [check_cpu] = $USER1$/check_pve.rb -s $HOSTADDRESS$ -u monitoring@pve -p $USER7$ -m cpu -n $HOSTNAME$ -w 70 -c 80
~~~

That's all to start with check_multi.
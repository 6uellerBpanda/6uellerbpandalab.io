---
title:  "Use Ansible to backup Kibana objects"
description: "Use Ansible to backup Kibana objects"
categories: ["Automation"]
image: "/assets/img/police_backup.jpg"
tags: ['kibana', 'ansible']
render_with_liquid: false
---

A quick way to export Kibana objects from spaces with Ansible.

* TOC
{:toc}

Here are the relevant variables:

~~~ yaml
kibana_url: 'https://kibana.example.com'

### spaces
kibana_spaces:
  - name: 'default'
  - name: 'marketing'
  - name: 'iot'

### backup
kibana_backup_dir: '/usr/share/kibana/backup'
kibana_backup_objects: ['config', 'map', 'canvas-workpad', 'canvas-element', 'lens', 'query', 'index-pattern', 'visualization', 'search', 'dashboard', 'url']
kibana_backup_cleanup_enabled: true
kibana_backup_cleanup_age: '4w'
~~~

For every Kibana space a folder will be created:

~~~ yaml
- name: configure backup folder
  file:
    path: "{{ kibana_backup_dir }}/{{ item.name }}"
    owner: "{{ kibana_user }}"
    group: "{{ kibana_group }}"
    mode: 0750
    state: directory
  loop: "{{ kibana_spaces }}"
~~~

The [Kibana API for exporting objects](https://www.elastic.co/guide/en/kibana/7.6/saved-objects-api-export.html) in respect
with spaces wasn't working with the "default" space so I used the ternary for this.
Maybe it's a bug but I didn't really check it.

This will create for every object in a space a file with the object name and a timestamp.

~~~ yaml
- name: backup saved objects
  uri:
    url: "{{ kibana_url }}/{{ (item.0.name == 'default') | ternary('api', 's/' ~ item.0.name ~ '/api') }}/saved_objects/_export"
    method: 'POST'
    user: "{{ kibana_api_username }}"
    password: "{{ kibana_api_password }}"
    body: { "type": "{{ item.1 }}" }
    body_format: 'json'
    force_basic_auth: true
    headers: { 'kbn-xsrf': 'backup' }
    return_content: true
    dest: "{{ kibana_backup_dir }}/{{ item.0.name }}/kibana_backup_{{ item.1 }}_{{ ansible_date_time.date }}.ndjson"
  loop: "{{ kibana_spaces | product(kibana_backup_objects) | list }}"
  loop_control:
    label: "{{ item.0.name }} - {{ item.1 }}"
~~~

This is an optional cleanup step 'cause after some time a lot files will be generated.
Of course an alternative would be also to zip the files.

~~~ yaml
- name: get old backups
  find:
    paths: "{{ kibana_backup_dir }}"
    age: "{{ kibana_backup_cleanup_age }}"
    file_type: 'file'
    use_regex: true
    patterns: '^kibana_backup_.*.ndjson'
    recurse: true
  register: backups_to_delete
  when: kibana_backup_cleanup_enabled

- name: cleanup old backups
  file:
    path: "{{ item.path }}"
    state: absent
  loop: "{{ backups_to_delete.files }}"
  when: kibana_backup_cleanup_enabled
~~~

---
Tested with:

* Ansible 2.7
* Kibana 7.6
